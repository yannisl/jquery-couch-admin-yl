'use strict';

$(document).ready(function() {
	Handlebars.registerHelper("debug", function(optionalValue) {
		console.log("Current Context");
		console.log("====================");
		console.log(this);

		if (optionalValue) {
			console.log("Value");
			console.log("====================");
			console.log(optionalValue);
		}
	});

	Handlebars.registerHelper("formatMoney", function(value) {
		return accounting.formatMoney(value, '');
	});

	$.whenAll = function(arr) {
		return $.when.apply($, arr).then(function() {
				return Array.prototype.slice.call(arguments);
		});
	};

	var App = {
		start: function() {
			this.materialsProvider = new MaterialsProvider();
			this.authProvider = new AuthProvider();
			this.departmentsProvider = new DepartmentsProvider();

			this.router = Router(routes);
			this.router.configure({
				html5history: true
			});

			var login = localStorage.getItem('login');
			var password = localStorage.getItem('password');
			var self = this;

			this.authProvider.login(login, password).then(function() {
				App.user.id = login;
				return this;
			}).always(function() {
				self.router.init();
			});
		},
		user: {
		},
		navigate: function(url) {
			this.router.setRoute(url);
		}
	};

	var routes = {
		'/': function() {
			var indexView = new IndexView(App);
			indexView.init().then(function(result) {
				indexView.render();
			});
		},
		'/login': function() {
			var loginView = new LoginView(App);
			loginView.render();
		},
		'/create': function() {
			var editView = new EditView(App);

			editView.fetchDepartments().then(function() {
				editView.render({})
			});
		},
		'/deleted': function() {
			var indexView = new IndexView(App);

			indexView.loadDeleted().then(function(result) {
				indexView.render();
			});
		},
		'/:id/edit': function(id) {
			var editView = new EditView(App);

			$.whenAll([
				editView.fetchData(id),
				editView.fetchDepartments()
			]).then(function() {
				editView.render();
			});
		},
	};

	$(document).ajaxError(function(event, jqxhr, settings, exception) {
		if (exception === 'Unauthorized') {
			localStorage.removeItem('login');
			localStorage.removeItem('password');

			if (App.router.getRoute() != 'login') {
				App.navigate('login');
			}
		}
	});

	App.start();
});
