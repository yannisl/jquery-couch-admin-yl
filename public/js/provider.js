'use strict';

var DataProvider = function() {
	this.database = 'materialsdb';
	this.url = 'http://yannisl.iriscouch.com';
};

DataProvider.prototype._buildUrl = function() {
	return [this.url, this.database].join('/');
};

DataProvider.prototype._makeRequest = function(data, type, url) {
	url = url || this._buildUrl();
	if (['post', 'put'].indexOf(type.toLowerCase()) !== -1) {
		data = JSON.stringify(data);
	}
	return $.ajax({
		url: url,
		contentType: 'application/json',
		data: data,
		dataType: 'json',
		type: type
	});
};

DataProvider.prototype.create = function(data) {
	return this._makeRequest(data, 'POST');
};

DataProvider.prototype.update = function(data) {
	var url = [this._buildUrl(), data._id].join('/');
	return this._makeRequest(data, 'PUT', url);
};

DataProvider.prototype._buildViewUrl = function(viewName) {
	return [this._buildUrl(), '_design', this.designDocument, '_view', viewName].join('/');
};

DataProvider.prototype.fetchView = function(viewName, params, options) {
	if (typeof params === 'string') {
		options = options || {};
		if (options.limit) {
			if (params) {
				params += '&';
			}
			params += 'limit=' + options.limit;
		}
		if (options.startKey) {
			if (params) {
				params += '&';
			}
			var startKey = options.startKey;
			if (typeof options.startKey == 'string') {
				startKey = '"' + startKey + '"';
			}
			params += 'startkey=' + startKey;
		}
		if (options.startKeyDocId) {
			if (params) {
				params += '&';
			}
			params += 'startkey_docid=' + options.startKeyDocId;
		}
		if (options.skip) {
			if (params) {
				params += '&';
			}
			params += 'skip=' + options.skip;
		}
	}
	return this._makeRequest(params, 'GET', this._buildViewUrl(viewName, options));
};

var MaterialsProvider = function() {
	this.designDocument = 'materials';
	DataProvider.apply(this, arguments);
};

Utils.extend(MaterialsProvider, DataProvider);

MaterialsProvider.prototype.searchByCode = function(code, options) {
	var query = code ? 'key="' + code + '"' : '';
	return this.fetchView('by_code', query, options);
};

MaterialsProvider.prototype.searchBySupplier = function(supplier, options) {
	var query =	query = supplier ? 'key="' + supplier + '"' : '';
	return this.fetchView('by_supplier', query, options);
};

MaterialsProvider.prototype.fetchDeleted = function(options) {
	return this.fetchView('deleted', '', options);
};

var DepartmentsProvider = function() {
	this.designDocument = 'departments';
	DataProvider.apply(this, arguments);
};
Utils.extend(DepartmentsProvider, DataProvider);

var AuthProvider = function() {
	DataProvider.apply(this, arguments);
};
Utils.extend(AuthProvider, DataProvider);

AuthProvider.prototype.login = function(login, password) {
	var baseAuthHeader = "Basic " + btoa(login + ":" + password);
	return $.ajax({
		type: "GET",
		url: this._buildUrl(),
		headers: {
			"Authorization": baseAuthHeader
		}
	}).then(function() {
		$.ajaxSetup({
			beforeSend: function(xhr) {
        xhr.setRequestHeader('Authorization', baseAuthHeader);
			}
		});
		return this;
	});
};

AuthProvider.prototype.logout = function() {
	localStorage.removeItem('login');
	localStorage.removeItem('password');
	$.ajaxSetup({
		beforeSend: function(xhr) {
		}
	});
};
