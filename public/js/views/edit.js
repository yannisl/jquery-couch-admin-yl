'use strict';

var EditView = function(app) {
	this.app = app;
	this.model = {
		type: 'material',
		internalApprovals: {},
		comments: [],
		transactions: [],
		loi: []
	};

	var source = $('#editView-template').html();
	this.template = Handlebars.compile(source);

	source = $('#commentsList-template').html();
	this.commentsListTemplate = Handlebars.compile(source);

	source = $('#transactionsList-template').html();
	this.transactionsListTemplate = Handlebars.compile(source);

	source = $('#loiList-template').html();
	this.loiListTemplate = Handlebars.compile(source);
};

EditView.prototype._initDatepickers = function() {
	var self = this;

	$('[name="requiredByFrom"]').pikaday({
		defaultDate: this.model.requiredBy ? Utils.requiredByToDate(this.model.requiredBy[0]): '',
		setDefaultDate: true,
		onSelect: function(date) {
			if (!self.model.requiredBy) {
				self.model.requiredBy = [];
			}
			self.model.requiredBy[0] = Utils.dateToRequiredBy(date);
		}
	});

	$('[name="requiredByTo"]').pikaday({
		defaultDate: this.model.requiredBy ? Utils.requiredByToDate(this.model.requiredBy[1]): '',
		setDefaultDate: true,
		onSelect: function(date) {
			if (!self.model.requiredBy) {
				self.model.requiredBy = [];
			}
			self.model.requiredBy[1] = Utils.dateToRequiredBy(date);
		}
	});

	$('[name="eta"]').pikaday({
		defaultDate: this.model.eta ? Utils.requiredByToDate(this.model.eta[0]): '',
		setDefaultDate: true,
		onSelect: function(date) {
			if (!self.model.eta) {
				self.model.eta = [];
			}
			self.model.eta[0] = Utils.dateToRequiredBy(date);
		}
	});

	$('[name="loiDate"]').pikaday({
		defaultDate: '',
		setDefaultDate: true
	});

	var approvals = ['mm1', 'mm2', 'pd', 'bod'];

	if (!this.model.internalApprovals) {
		this.model.internalApprovals = {};
	}

	for (var i in approvals) {
		var approve = approvals[i];
		var approveValue = this.model.internalApprovals[approve];
		(function(approve, approveValue) {
			$('[name="' + approve + 'Approval"]').pikaday({
				defaultDate: approveValue ? Utils.dottedDateToDate(approveValue) : '',
				setDefaultDate: true,
				onSelect: function(date) {
					self.model.internalApprovals[approve] = Utils.dateToDottedDate(date);
				}
			});
		})(approve, approveValue);
	}

};

EditView.prototype._initControls = function() {
	$('.hide-list-control__toggle').on('click', function() {
		var currentState = $(this).data('state');

		var items = $(this).parents('.hide-list-control').find('.hide-list-control__items');

		if (currentState === 'hidden') {
			$(this).data('state', 'showed')
			$(this).text('hide');
			items.show();
		} else {
			$(this).data('state', 'hidden')
			$(this).text('show');
			items.hide();
		}

		return false;
	});

	var self = this;

	$('.comment-add').on('click', function() {
		var text = $('[name="commentText"]').val();


		if (!self.model.comments) {
			self.model.comments = [];
		}

		self.model.comments.push({
			userid: self.app.user.id,
			comment: text,
			date: Utils.dateToDottedDate(new Date())
		});

		$('[name="commentText"]').val('');

		self._renderComments();

		return false;
	});

	$('.loi-add').on('click', function() {
		var text = $('[name="loiRef"]').val();
		var pikaday = $('[name="loiDate"]').data('pikaday');
		var date = pikaday.getDate();

		if (!self.model.loi) {
			self.model.loi = [];
		}

		self.model.loi.push({
			ref: text,
			date: Utils.dateToDottedDate(date)
		});

		$('[name="loiRef"]').val('');
		pikaday.setDate('');

		self._renderLoi();

		return false;
	});


};

EditView.prototype._renderComments = function() {
	$('.comments-l').empty();
	$('.comments-l').html(this.commentsListTemplate({items: this.model.comments}));

	$('.comment-wrap').hover(function() {
		$(this).find('.comment__delete-control').show();
	}, function() {
		$(this).find('.comment__delete-control').hide();
	});

	var self = this;

	$('.comment__delete').on('click', function() {
		var index = parseInt($(this).data('index'), 10);

		self.model.comments.splice(index, 1);
		self._renderComments();

		return false;
	});
};

EditView.prototype._renderLoi = function() {
	$('.loi-l').empty();
	$('.loi-l').html(this.loiListTemplate({items: this.model.loi}));

	$('.loi-wrap').hover(function() {
		$(this).find('.loi__delete-control').show();
	}, function() {
		$(this).find('.loi__delete-control').hide();
	});

	var self = this;

	$('.loi__delete').on('click', function() {
		var index = parseInt($(this).data('index'), 10);

		self.model.loi.splice(index, 1);
		self._renderLoi();

		return false;
	});
};

EditView.prototype._renderTransactions = function() {
	$('.transactions-l').empty();
	$('.transactions-l').html(this.transactionsListTemplate({items: this.model.transactions}));
};


EditView.prototype._toggleDeleteState = function() {
	this.model.deleted = !this.model.deleted;
	this.model.lasteditedby = this.app.user.id;

	var self = this;
	return this.app.materialsProvider.update(this.model).then(function() {
		self.app.navigate('/');
	});
};

EditView.prototype._initValidators = function() {
	var self = this;

	$('.material-edit-form').bootstrapValidator({
		message: 'This value is not valid',
		fields: {
			code: {
				validators: {
					notEmpty: {
						message: 'The code is required and cannot be empty'
					},
					regexp: {
							regexp: /^[^{}<>\[\]]+$/,
							message: 'The description can only consist of text characters'
					}
				}
			},
			description: {
				message: 'The description is not valid',
				validators: {
						notEmpty: {
								message: 'The description is required and cannot be empty'
						},
						stringLength: {
								min: 6,
								max: 180,
								message: 'The description must be more than 6 and less than 180 characters long'
						},
						regexp: {
								regexp: /^[^{}<>\[\]]+$/,
								message: 'The description can only consist of text characters'
						}
				}
			},
			shortDescription: {
				message: 'The short description is not valid',
				validators: {
						notEmpty: {
								message: 'The short description is required and cannot be empty'
						},
						stringLength: {
								min: 2,
								max: 30,
								message: 'The short description must be more than 2 and less than 30 characters long'
						},
						regexp: {
								regexp: /^[^{}<>\[\]]+$/,
								message: 'The short description can only consist of text characters'
						}
				}
			},
			budget: {
				validators: {
					notEmpty: {
						message: 'The budget is required and cannot be empty'
					},
						regexp: {
							regexp: /^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*\.[0-9]{2}$/,
							message: 'The budget should be formatted as ##,###,###.##'
						}
				}
			},
			supplier: {
				validators: {
					notEmpty: {
						message: 'The supplier is required and cannot be empty'
					}
				}
			},
			commentText: {
				validators: {
					notEmpty: {
						message: 'The comment text is required and cannot be empty'
					}
				}
			}
		},
		submitHandler: function(validator, form, submitButton) {
			var isNew = self.model._id ? false : true;
			var material = self.model || { type: 'material' };

			material.description = $(form).find('[name="description"]').val().trim();
			material.shortDescription = $(form).find('[name="shortDescription"]').val().trim();
			material.supplier = $(form).find('[name="supplier"]').val().trim();
			material.department = $(form).find('[name="department"]').val();
			material.class = $(form).find('[name="class"]').val().trim();
			material.code = $(form).find('[name="code"]').val().trim();
			material.budget = accounting.unformat($(form).find('[name="budget"]').val());
			material.longLead = $(form).find('[name="longLead"]:checked').val() === 'true';
			material.lasteditedby = self.app.user.id;

			var successCallback = function(result) {
				self.model._id = result.id;
				self.model._rev = result.rev;
				$('.result-alert').addClass('alert-success');
				$('.result-alert .alert__text').text('Material was saved successfully');
				$('.result-alert').show();

				if (isNew) {
					self.app.navigate(result.id + '/edit')
				}
			};

			var failureCallback = function(result) {
				$('.result-alert').addClass('alert-warning');
				$('.result-alert .alert__text').text('Error was happen when saving');
				$('.result-alert').show();
			};

			if (isNew) {
				self.app.materialsProvider.create(material).then(successCallback, failureCallback);
			} else {
				self.app.materialsProvider.update(material).then(successCallback, failureCallback);
			}

			return false;
		}
	});
};

EditView.prototype.render = function() {
	var self = this;

	$('.page-layout').empty();
	$('.page-layout').append(this.template({
		model: this.model,
		departments: this.departments.map(function(department) {
			return {
				name: department,
				selected: self.model.department === department
			};
		})
	}));

	$('.alert__close').on('click', function() {
		$(this).parents('.alert').hide();
	});

	this._renderComments();
	this._renderLoi();
	this._renderTransactions();

	this._initDatepickers();
	this._initControls();
	this._initValidators();

	$('.material-delete-form').submit(function(e) {
		self._toggleDeleteState();
		return false;
	});

	$('.material-restore-form').submit(function(e) {
		self._toggleDeleteState();
		return false;
	});
};

EditView.prototype.fetchDepartments = function() {
	var self = this;
	return this.app.departmentsProvider.fetchView('names').then(function(result) {
		self.departments = result.rows[0].value;
		return this;
	});
};

EditView.prototype.fetchData = function(id) {
	var preparedId = '"' + id + '"';
	var self = this;
	return this.app.materialsProvider.fetchView('by_id', { key: preparedId }).then(function(result) {
		self.model = result.rows[0].value;
		return this;
	});
};
