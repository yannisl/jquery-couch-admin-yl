'use strict';

var IndexView = function(app) {
	this.app = app;
	this.limit = 30;
	this.previousPages = [];

	var source = $('#indexView-template').html();
	this.template = Handlebars.compile(source);

	var source = $('#indexViewItems-template').html();
	this.itemsTemplate = Handlebars.compile(source);

	var source = $('#pagination-template').html();
	this.paginationTemplate = Handlebars.compile(source);
};

IndexView.prototype.init = function() {
	return this._search('byCode');
};

IndexView.prototype.loadDeleted = function() {
	var self = this;
	return this.app.materialsProvider.fetchDeleted().then(function(result) {
		self.items = result.rows;
		self._renderItems();
	});
};

IndexView.prototype._renderItems = function() {
	$('.items-l').empty();
	$('.items-l').append(this.itemsTemplate({items: this.items}));

	var self =  this;

	$('.link-to').on('click', function() {
		self.app.navigate($(this).data('href'));
		return false;
	});

	this._initPagination();
};

IndexView.prototype._search = function(searchBy, query, options) {
	options = options || {};
	options.limit = options.limit || this.limit;
	options.limit += 1;
	options.skip = 0;
	options.startKey = this.startKey;
	options.startKeyDocId = this.startKeyDocId;
	var searchPromise;

	switch (searchBy) {
		case 'byCode':
			searchPromise = this.app.materialsProvider.searchByCode(parseInt(query, 10), options);
			break;
		case 'byDescription':
			searchPromise = this.app.materialsProvider.searchByDescription(query, options);
			break;
		case 'bySupplier':
			searchPromise = this.app.materialsProvider.searchBySupplier(query, options);
			break;
	};

	var self = this;
	return searchPromise.then(function(result) {
		var lastElement = result.rows[self.limit];
		self.startKey = lastElement ? lastElement.key : '';
		self.startKeyDocId = lastElement ? lastElement.id : '';
		self.items = result.rows.slice(0, self.limit);
	});
};

IndexView.prototype._initPagination = function() {
	this.currentPage = 0;

	$('.pagination-l').empty();
	$('.pagination-l').append(this.paginationTemplate({
		hasNext: this.startKey,
		hasPrevious: this.previousPages.length,
		hasPages: this.startKey || this.previousPages.length
	}));

	var self = this;
	var loadWithPage = function() {
		self._search(self._getSearchBy(), self._getQuery(), {
			limit: self.limit
		}).then(function() {
			self._renderItems();
		});
	};

	$('.pager__next').on('click', function() {
		self.previousPages.push({
			key: self.items[0].key,
			id: self.items[0].id
		});
		loadWithPage();
	});
	$('.pager__prev').on('click', function() {
		var res = self.previousPages.pop();
		self.startKey = res.key;
		self.startKeyDocId = res.id;
		loadWithPage();
	});
};

IndexView.prototype._getSearchBy = function() {
	return $('.search-by').val();
};

IndexView.prototype._getQuery = function() {
	return $('.search').val();
};

IndexView.prototype.render = function() {
	$('.page-layout').empty();
	$('.page-layout').append(this.template());
	
	var self = this;

	$('.signout').on('click', function() {
		// TODO this is look strange but it do the trick we remove our header adding
		self.app.authProvider.logout();
		self.app.navigate('/login');
	});

	$('.filter__deleted__button').on('click', function() {
	});

	$('.search-by-alternatives li a').click(function(){
		self.startKey = '';
		self.startKeyDocId = '';
		self.previousPages = [];

		$('.search').val('');

		$(this).parents(".input-group-btn").find('.btn .text').text($(this).text());
		$(this).parents(".input-group-btn").find('.btn').val($(this).data('value'));

		var searchBy = self._getSearchBy();
		self._search(searchBy).then(function() {
			self._renderItems();
		});
	});

	$('.filter').submit(function() {
		return false;
	});

	this._renderItems();

	$('.search').on('keyup', function() {
		self.startKey = '';
		self.startKeyDocId = '';
		self.previousPages = [];

		var query = self._getQuery()
		var searchBy = self._getSearchBy();

		clearTimeout(self.searchTimeout);

		self.searchTimeout = setTimeout(function() {
			self._search(searchBy, query).then(function() {
				self._renderItems();
			});
		}, 400);

		return false;
	});
};
