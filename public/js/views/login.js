'use strict';

var LoginView = function(app) {
	this.app = app;
	var source = $('#login-template').html();
	this.template = Handlebars.compile(source);
};

LoginView.prototype.login = function() {
	var login = $('[name="login"]').val();
	var password = $('[name="password"]').val();

	var self = this;
	this.app.authProvider.login(login, password).then(
		function() {
			localStorage.setItem('login', login);
			localStorage.setItem('password', password);
			self.app.user.id = login;

			self.app.navigate('/');

			return this;
		},
		function() {
			$('.error-message').show();
		}
	);

	return false;
};

LoginView.prototype.render = function() {
	$('.page-layout').empty();
	$('.page-layout').append(this.template());

	$('.login-form__submit').on('click', this.login.bind(this));

	$('[name="login"]').on('keyup', function() {
		$('.error-message').hide();
	});
	$('[name="password"]').on('keyup', function() {
		$('.error-message').hide();
	});
};
